TARGET = voide

SOURCES     += src/*.cc
INCLUDEPATH += ./inc/

QT = core gui

DESTDIR = ./bin/
CONFIG += object_parallel_to_source

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
