#ifndef __MAIN_WINDOW__
#define __MAIN_WINDOW__

#include <QMainWindow>
#include <QHBoxLayout>

/*
The main window containing 
*/
class MainWindow : public QMainWindow
{
public:
     MainWindow();
    ~MainWindow();

private:
    QWidget     main_widget;
    QHBoxLayout main_layout;
};

#endif