TARGET = voide

SOURCES += src/*.cc
HEADERS += inc/*.hh

QT = core

DESTDIR = ./bin/
CONFIG += object_parallel_to_source

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
